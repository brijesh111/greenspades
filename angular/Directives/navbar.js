/**
 * Created by brijesh on 16/09/17.
 */

"use strict";

var app = angular.module('greenSpades');

/**
 *  Directive for content slider
 */
app.directive('navbar',function(){
    return{
        strict:'E',
        templateUrl:'html/navbar.html',
        controller:'NavigationController'

    }

});
