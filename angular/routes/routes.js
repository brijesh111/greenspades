/**
 * Created by brijesh on 11/09/17.
 */

"use strict";

var app = angular.module('greenSpades');

app.config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider, $cssProvider) {

        $locationProvider.html5Mode({
            enabled: true
        });

        $routeProvider.when('/', {
            templateUrl: 'html/login.html',
            controller: 'LoginController'

            })
            .when('/newTransaction', {
                templateUrl: 'html/newTransaction.html',
                controller: 'NewTransactionController'

            })
            .when('/customer', {
                templateUrl: 'html/customer.html',
                controller: 'CustomerController'

            })
            .when('/addOffer/:redirectpath', {
                templateUrl: 'html/addOffer.html',
                controller: 'AddOfferController'
            })
            .when('/makeSale', {
                templateUrl: 'html/makeSaleforforCustomer.html',
                controller: 'NewMakeSaleController'

            })
           .when('/updateProfile', {
            templateUrl: 'html/updateProfile.html',
            controller: 'UpdateProfileController'

          })
            .when('/userRewarded', {
                templateUrl:"html/userRewarded.html",
                controller:"userRewardedController"
            })
            .when('/apiFeedback', {
                templateUrl:"html/apiFeedback.html",
                controller:"ApiFeedbackController"
            })
            .when('/optionOut', {
                templateUrl:"html/optionOut.html",
                controller:"OptionOutController"
            })
            .when('/thankYou', {
                templateUrl:"html/thankYou.html",
                controller:"CallBackPageController"
            })
            .when('/unsubscribe', {
                templateUrl:"html/unsubscribe.html",
                controller:"CallBackPageController"
            })

            .otherwise({
                redirectTo: '/'
            });

    }
]);


app.run(['$rootScope', '$location', function ($rootScope, $location) {
  $rootScope.$on('$routeChangeStart', function (event, pv, current) {
      var withoutPermissionState = ['/userRewarded', '/apiFeedback','/thankYou', '/unsubscribe', '/optionOut'];
      if (!sessionStorage.getItem('loginData') && withoutPermissionState.indexOf($$route.originalPath) === -1) {
        $location.path('/');
    }
  });
}]);
