/**
 * Created by rajeshpatel on 25/11/17.
 */
'use strict';

var app = angular.module('greenSpades');

app.controller("OptionOutController",function($scope, $http, $location, ENV) {
  var merchant_id = $location.search().b;
  var mobile = $location.search().a;
  var track = angular.isDefined($location.search().t) ? $location.search().t : undefined;
  var trackingURL = $location.absUrl();
  $scope.merchantData = [];

    if (track) {
      var data = { "mobileNumber": mobile, "merchantId": merchant_id, "type": track, "shortUrl": trackingURL };
      $http.post(ENV().API_ENDPOINT + "v1/link/track/add", data)
          .then(function sucess(data) {
          }, function error(err) {
              console.log(err);
          })
    }
  console.log($scope);

  $scope.isDataLoaded = false;
  $scope.isDataNotFound = false;
  $scope.isLoaded = true;

  $scope.getMerchantData= function(){
    $http.get(ENV().API_ENDPOINT+"v1/merchant/"+merchant_id+"/feedback/data")
      .then(function sucess(data){
        console.log(data);
        $scope.isDataLoaded = true;
        $scope.isLoaded = false;
        $scope.merchantData = data.data.data.merchantData;
      }, function error(err){
        console.log(err);
        $scope.isDataLoaded = true;
        $scope.isDataNotFound = true;
        $scope.isLoaded = false;
      })
  };

  $scope.getMerchantData();

  $scope.submitOptionOut  = function(){

     var data = {
       merchantId:merchant_id,
       mobileNumber:JSON.stringify($scope.mobileNumber)
    }
    console.log('$scope.mobileNumber == mobile', $scope.mobileNumber ,'==', mobile);
    if(angular.isDefined($scope.mobileNumber) && $scope.mobileNumber == mobile) {
      $scope.loader = true;
      $http.post(ENV().API_ENDPOINT+"v1/customer/optout",data)
        .then(function success(data){
          $scope.loader = false;
          $scope.mobileNumber = "";
          $location.path('/unsubscribe');
        },function error(err){
          $scope.loader = false;
          swal("Something went wrong!!");
          console.log(err);
        })
    }else {
      swal("Please Enter Valid Number");
    }


  };

});