/**
 * Created by brijesh on 13/09/17.
 */


var app = angular.module('greenSpades');

app.controller('NewTransactionController', function ($scope,$location,$http,$cookies,$rootScope,ENV,$cookies,billAmount,$timeout, $q, $log, $window) {


    $timeout(function() {
        var inputs = document.querySelectorAll('md-autocomplete[md-input-maxlength]');
        for (var i = 0; i < inputs.length; i++) {
            var minLength = inputs[i].attributes['md-input-minlength'].value,
                maxLength = inputs[i].attributes['md-input-maxlength'].value,
                input = inputs[i].getElementsByTagName('INPUT')[0];
            input.setAttribute('minlength', minLength);
            input.setAttribute('maxlength', maxLength);
        }
    }, 500, false);


    if($cookies.getObject('loginCreditialscookie')){
        var customerCredentials=$cookies.getObject('loginCreditialscookie');

    }else {
        $location.path('/');
    }

    var loginData = JSON.parse(sessionStorage.getItem('loginData'));


    if(!loginData){
        $location.path('/');
    }

    if(loginData.data.transactions){

        $scope.transactionsData=loginData.data.transactions;

    }

    $scope.kpidata = loginData.data.kpis;

    billAmount.add(0,0);

    if(sessionStorage.getItem('customerData')){
      sessionStorage.removeItem('customerData');
    }

    $window.localStorage.setItem("orderCacheData", '');

    if(sessionStorage.getItem('offerSelected')){
        sessionStorage.removeItem('offerSelected');
    }

    if(sessionStorage.getItem('notifyUser')){
        sessionStorage.removeItem('notifyUser');
    }
  
   if(sessionStorage.getItem('BillAmount')){
        sessionStorage.removeItem('BillAmount');
    }

    if(sessionStorage.getItem('billNumberData')){
        sessionStorage.removeItem('billNumberData');
    }


    // console.log(loginData.data);

    $scope.customer = {};

    $scope.customerDetails = function () {

        var data = {
            agentId: Number(customerCredentials.agentId),
            accessToken: customerCredentials.accessToken,
            mobileNumber:$scope.customer.phoneNumber.toString(),
            deviceType: 2,
            isVisit:true

        };
        $rootScope.loader = true;

        $http.post(ENV().API_ENDPOINT+"v1/customer/get",data)
            .then(function successCallback(response){
                $rootScope.loader = false;
                sessionStorage.setItem('customerData', JSON.stringify(response));
                $location.path('/customer');
              },function errorCallback(err) {
                $rootScope.loader = false;
                console.log(err);
                swal(err.data.message)

            });

    };


    $scope.makeSale = function () {

        console.log($scope.customer.phoneNumber);
        var data = {
            agentId: Number(customerCredentials.agentId),
            accessToken: customerCredentials.accessToken,
            mobileNumber:$scope.customer.phoneNumber.toString(),
            deviceType: 2
        };
        $rootScope.loader = true;




        $http.post(ENV().API_ENDPOINT+"v1/customer/get",data)
            .then(function successCallback(response){
                $rootScope.loader = false;
                console.log(response);
                sessionStorage.setItem('customerData', JSON.stringify(response));

                $location.path('/makeSale');


                //$scope.getCustomers();
            },function errorCallback(err) {
                $rootScope.loader = false;
                console.log(err);
                swal(err.data.message)

            });

    };


    $scope.coloo = ['#9B71D5','#45C0E5','#FFA000','#EB3E7A'];

    console.log($scope.coloo);
    console.log($scope.coloo[0]);


    $('#gobutton1').attr("disabled", 'true');
    $('#gobutton2').attr("disabled", 'true');



    $scope.simulateQuery = false;
    $scope.isDisabled    = false;

    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    var data = {
        agentId: Number(customerCredentials.agentId),
        accessToken: customerCredentials.accessToken
    };
    $http.post(ENV().API_ENDPOINT+"v1/customerList/get", data)
    .then(function successCallback(response){
        console.log(response);
      $scope.customerAutoDetails = response.data;
    });

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for repos... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
      return query ? $scope.customerAutoDetails.filter(createFilterFor(query)) : $scope.customerAutoDetails;
    }

    function searchTextChange(text) {
      if(angular.isDefined(text)) {
        validationOnNumber(text);
        $scope.customer.phoneNumber = text;
      } else {
        validationOnNumber(0);
      }
    }

    function selectedItemChange(item) {
      if(angular.isDefined(item)) {
        validationOnNumber(item.MobileNumber);
        $scope.customer.phoneNumber = item.MobileNumber;
      } else {
          validationOnNumber(0);
      }
    }

      /**
       * Create filter function for a query string
       */
      function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(item) {
          return (angular.isDefined(item.Name) && item.Name.toLowerCase().indexOf(lowercaseQuery) === 0) || (angular.isDefined(item.MobileNumber) && (item.MobileNumber.indexOf(lowercaseQuery) === 0));
        };
      }

      function validationOnNumber(mobileNumber) {
        var reg = new RegExp('^[0-9]+$');
        if(mobileNumber.length == 10 && reg.test(mobileNumber)) {
          angular.element('#gobutton1').removeClass('gobutton');
          angular.element('#gobutton2').removeClass('gobutton');

          angular.element('#gobutton1').removeAttr('disabled');
          angular.element('#gobutton2').removeAttr('disabled');

        } else {
          angular.element('#gobutton1').addClass('gobutton');
          angular.element('#gobutton2').addClass('gobutton');


          angular.element('#gobutton1').attr("disabled", 'true');
          angular.element('#gobutton2').attr("disabled", 'true');
        }
      }


    $(document).ready(function() {
        jQuery.extend( jQuery.fn.dataTableExt.oSort, {
            "date-uk-pre": function ( a ) {
                var d = moment(a).format('DD/MM/YYYY');
                a = d;
                if (a == null || a == "") {
                  return 0;
                }
                var ukDatea = a.split('/');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },
         
            "date-uk-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },
         
            "date-uk-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        });
        $('#example').DataTable({
          "order": [[ 0, "desc" ]],
          "aoColumns": [
            null,
            { type: 'date-uk', targets: 0 },
            null,
            null,
            null,
            null,
            null,
            null
          ],
          "fnDrawCallback" : function(oSettings) {
            $('.paginate_button.previous.disabled').css('display', 'none');
            $('.paginate_button.next.disabled').css('display', 'none');
          }
        });
    });
});


