/**
 * Created by brijesh on 11/09/17.
 */

var app = angular.module('greenSpades');

app.controller('LoginController', function ($scope,$location,$rootScope,$http,ENV,$cookies, $window) {

    $scope.shop = { formData: {}};

    $scope.showPassword = function () {
        var p = document.getElementById('pwd');
        p.setAttribute('type', 'text');
    };

    // $scope.hidePassword =function () {
    //     var p = document.getElementById('pwd');
    //     p.setAttribute('type', 'password');
    // }

    $scope.shop.submit = function () {

        var data = {
            agentId: $scope.shop.formData.name,
            password: $scope.shop.formData.password,
            lite: true
        }

        $cookies.putObject("userAccountData",data);

        $cookies.getObject('userAccountData');

        console.log(data);

        $rootScope.loader = true;

        $http.post(ENV().API_ENDPOINT+"v1/agent/login",data)
            .then(function successCallback(response){
                $rootScope.loader = false;
                console.log(response);
                console.log(response.data);
                sessionStorage.setItem('loginData', JSON.stringify(response));
                var loginCreditials = {
                    accessToken : response.data.accessToken,
                    agentId : response.data.agentId
                };
                $cookies.putObject('loginCreditialscookie', loginCreditials);
                $window.localStorage.setItem("offlineItems", '');
                $location.path('/newTransaction');
            },function errorCallback(err) {
                $rootScope.loader = false;
                console.log(err);
                if(err.data&&err.data.message)
                    swal(err.data.message)

            });

    }
    
});