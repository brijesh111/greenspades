/**
 * Created by rajeshpatel on 24/11/17.
 */
'use strict';

var app = angular.module('greenSpades');

app.controller("userRewardedController",function($scope,$http,$location,ENV) {
  //angular.element('#gs-navbar').hide();
    console.log('$location', $location);
  $scope.mobileInputVisible = true;
  var customerId = $location.search().a;
  var merchantId = $location.search().b;
  var Mobile  = angular.isDefined($location.search().c) ? $location.search().c : undefined;
  var agentId = angular.isDefined($location.search().d) ? parseInt($location.search().d) : undefined;
  var accessToken = angular.isDefined($location.search().e) ? $location.search().e : undefined;
  var ReferralMobile = angular.isDefined($location.search().f) ? $location.search().f : undefined;
  var track = angular.isDefined($location.search().t) ? $location.search().t : undefined;
  var trackingURL = $location.absUrl();
    if (track) {
        var data = {
            "customerId": customerId, "mobileNumber": Mobile, "merchantId": merchantId,
            "type": track, shortUrl: trackingURL
        };
        $http.post(ENV().API_ENDPOINT + "v1/link/track/add", data)
            .then(function sucess(data) {
                console.log(data);
            }, function error(err) {
                console.log(err);
            })
    }

  $scope.isDataLoaded = false;
  $scope.isDataNotFound = false;
  $scope.isLoaded = true;
  $scope.userRewarded = [];

  $scope.getUserRewarded= function(customerId){
    $http.get(ENV().API_ENDPOINT+"v1/customer/data/"+customerId+"/"+merchantId)
      .then(function sucess(data){
        console.log(data);
        $scope.isDataLoaded = true;
        $scope.isLoaded = false;
        $scope.userRewarded = data.data.data.customerData;
        $scope.userCashbacks = data.data.data.cashbacks;
        $scope.merchantData = data.data.data.customerData.merchantData;
        $scope.tabs = data.data.data.tabNames;
        $scope.userIncentives = data.data.data.incentives;
        $scope.footerText = (data.data.data.footerText) ? data.data.data.footerText : '';
      }, function error(err){
        console.log(err);
        $scope.isDataLoaded = true;
        $scope.isDataNotFound = true;
        $scope.isLoaded = false;
      })
  };

  $scope.getUserRewardedReferral= function(){
    var data = { "agentId": agentId, "accessToken": accessToken,
      "mobileNumber": Mobile , "refreeMobNo": ReferralMobile};
    $http.post(ENV().API_ENDPOINT+"v1/customer/get", data)
      .then(function sucess(data){
        console.log(data);
        $scope.customerId = data.data._id;
        $scope.getUserRewarded($scope.customerId);
        console.log('=====',$scope.customerId);
      }, function error(err){
        console.log(err);
        $scope.isDataLoaded = true;
        $scope.isDataNotFound = true;
        $scope.isLoaded = false;
      })
  };

   $scope.getUserRewardedReferraly= function(){
    var data = { "agentId": agentId, "accessToken": accessToken,
      "mobileNumber": $scope.mobileNumber , "refreeMobNo": ReferralMobile};
    $http.post(ENV().API_ENDPOINT+"v1/customer/get", data)
      .then(function sucess(data){
         $scope.mobileInputVisible = true;
        console.log(data);
        $scope.customerId = data.data._id;
        $scope.getUserRewarded($scope.customerId);
        console.log('=====',$scope.customerId);
      }, function error(err){
        console.log(err);
        $scope.isDataLoaded = true;
        $scope.isDataNotFound = true;
        $scope.isLoaded = false;
      })
  };

  if (angular.isDefined(Mobile) && angular.isDefined(ReferralMobile) ) {
    $scope.getUserRewardedReferral();
  }
  else if (!angular.isDefined(Mobile) && angular.isDefined(ReferralMobile) ) {
    $scope.mobileInputVisible = false;
    $scope.isLoaded = false;
  }
  else {
    $scope.getUserRewarded(customerId);
  }

});