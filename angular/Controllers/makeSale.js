var app = angular.module('greenSpades');

app.controller('NewMakeSaleController', function ($scope,$location,$http,$cookies,$rootScope,ENV,$cookies,$window,billAmount, updateTransactionsBills) {
    
    var loginData = (JSON.parse(sessionStorage.getItem('loginData'))).data;
    var customerData = (JSON.parse(sessionStorage.getItem('customerData'))).data;
    var customerCredentials=$cookies.getObject('loginCreditialscookie');
    $scope.salesBillPrint = true;
    $scope.customerData = customerData;
    $scope.customerData.DOB = ($scope.customerData.DOB)? new Date($scope.customerData.DOB) : undefined;
    $scope.customerData.AnnivDate = ($scope.customerData.AnnivDate)? new Date($scope.customerData.AnnivDate) : undefined;
    $scope.flatDiscount = 0;

  //angular.isObject(customerData.promoCode) ? true : false;

    $scope.customerPromoCode = false;

    $scope.salesDate = new Date();
    $scope.merchantName = loginData.name;
    $scope.customer = {};
    $scope.allItems = {};
    $scope.totalAmount = 0;
    $scope.isPromoCodeApplied = false;
    $scope.isDefaultPromoCodeApplied = true;
    $scope.selectedItems = 0;
    if (!angular.isObject($scope.customerData.promoCode)) {
      $scope.isPromoCode = false;
      $scope.isPromoCodeApplied = true ;
      $scope.isDefaultPromoCodeApplied = false;
      }else{
      isPromoCodeApplied = false ;
    }
    $rootScope.loader = false;
    $scope.promoCodeDiscount = 0;
    $scope.totalTaxAmount = 0;

    $scope.customer.name = customerData.Name;
    $scope.customer.email = customerData.Email? customerData.Email : null;  //not in response
    $scope.customer.phone = customerData.MobileNumber;
    $scope.customer.id = customerData._id? customerData._id : null;
    $scope.spadesAvailable = customerData.spades ?  customerData.spades : 0;
    console.log('spades', $scope.spadesAvailable);
    $scope.sales = {
        notifyUser: (($window.sessionStorage.notifyUser && $window.sessionStorage.notifyUser != 'false') ? true : false)
    };
    $scope.spadesUsed = 0;//$scope.customer.spadesAvailable;
    $scope.amoutToBeDetucted = Number(0);
    if(loginData.accessToken){
        $rootScope.agentId = loginData.agentId;
        $rootScope.accessToken = loginData.accessToken;
        $scope.tinNumber = loginData.tinNumber || '';
        $scope.address = loginData.address;
        $scope.billMobileNumber = loginData.billMobileNumber;
    }else{

        $rootScope.agentId = null;
        $rootScope.accessToken = null;
        $scope.username = null;
        $location.path("/");
    }



    if(loginData){
        $scope.shopStoreTaxData = loginData.taxes;
        console.log("merchant taxes get");
        console.log($scope.shopStoreTaxData);

    }

    if($scope.customerData.isNewCustomer) {
        angular.element('#profileUpdateModal').modal('show');
    }

    $scope.getAllItems = function(){

        if($window.localStorage.getItem("orderCacheData")){
             $scope.allItems = JSON.parse($window.localStorage.getItem("orderCacheData"));
             console.log("cache datatest");
             $scope.refreshCheckoutCalculation();
        } else if($window.localStorage.getItem("offlineItems")){

            $scope.allItems = JSON.parse($window.localStorage.getItem("offlineItems"));
            console.log("Offline content jjj");
            console.log($scope.allItems);
 
        } else {
            $rootScope.loader = true;
            $http.get(ENV().API_ENDPOINT + "v1/" + $rootScope.agentId + "/" + $rootScope.accessToken + "/items/get")
                .then(function successCallback(response) {
                    console.log(response);
                    $scope.allItems = response.data.data;
                    $rootScope.loader = false;
                    $scope.processAllItems();
                }, function errorCallback(err) {
                    console.log(err);
                    $rootScope.loader = false;
                });
        }
    };


    var discount = 0;
    // var totalTax = 0;
    var itemCost = 0;
    var promoAmount = 0;
    var taxAmount = 0;

    $scope.calculatedDiscount = 0;
    $scope.calculatePromoCodeAmount = function (newBillAmount) {
        console.log("----------In calculatePromoCodeAmount itemCost------------"+itemCost);

        console.log(newBillAmount)
        console.log($scope.customerData);
        itemCost = newBillAmount;

        console.log("In calculatePromoCodeAmount itemCost "+itemCost);
        console.log("In calculatePromoCodeAmount ");
        if ($scope.customerData.promoCode && ($scope.customerData.promoCode.mintTotalAmount <= newBillAmount)) {

            //$scope.isDefaultPromoCodeApplied = true;
            console.log("In calculatePromoCodeAmount ");
            promoAmount = (newBillAmount) - (newBillAmount) / (Number($scope.customerData.promoCode.amount + 1)); //promoAmount calculated
            console.log("promoAmount" + promoAmount);

            if ($scope.customerData.promoCode.type == 1) {

                discount = Math.min($scope.customerData.promoCode.amount, itemCost);
                console.log("$scope.customerData.promoCode.type == 1 console.log(discount)" + discount)
            }
            else {
                console.log("promoAmount * itemCost"+$scope.customerData.promoCode.amount * itemCost)
                discount = Math.min(($scope.customerData.promoCode.amount * itemCost), itemCost)
                console.log("Percent console.log(discount)" + discount)
            }

        } else {
            //$scope.isDefaultPromoCodeApplied = false;

            discount = 0;
            console.log("outer else console.log(discount)" + discount)
        }
        return discount

    }

    $scope.processAllItems =  function(){
        for(var i in $scope.allItems){
            $scope.allItems[i].quantity = 0;
            $scope.selectedItems = 0;
        }


        $window.localStorage.setItem("offlineItems",JSON.stringify($scope.allItems))

    };

    $scope.addItem = function(index){
        console.log("test", $scope.allItems, index);
        $scope.allItems[index].quantity = $scope.allItems[index].quantity + 1;
        $scope.refreshCheckoutCalculation();
        $scope.cacheOrderItemData();
        $scope.removeSelectedSaleOffer();
    };

    $scope.cacheOrderItemData = function() {
      $window.localStorage.setItem("orderCacheData",JSON.stringify($scope.allItems));
    }

    $scope.refreshCheckoutCalculation =  function() {
        $scope.totalAmount = 0; $scope.selectedItems = 0; $scope.totalTaxAmount = 0; $scope.amountToBePaid = 0;
        angular.forEach($scope.allItems, function(obj, key) {
            if(obj.quantity) {
                $scope.selectedItems = $scope.selectedItems + 1;
                $scope.totalAmount = $scope.totalAmount + (obj.quantity * obj.itemPrice);
            }
        });
        var itemCost = $scope.totalAmount;
        for(var i in $scope.allTaxes) {
            var amountAdded = ($scope.selectedItems > 0) ? ($scope.totalAmount * $scope.allTaxes[i].taxPercent) : 0;
            $scope.allTaxes[i].amount = amountAdded;
            if($scope.selectedItems <= 0){ $scope.allTaxes[i].amount = 0; }
            $scope.totalAmount = $scope.totalAmount + amountAdded;
            $scope.totalTaxAmount += amountAdded;
        }
        if($scope.flatDiscount > itemCost) {
          swal("Flat Discount greater than total amount");
        }
        $scope.calculatedDiscount = ($scope.calculatedOfferType(($scope.totalAmount), $scope.totalTaxAmount)) + ($scope.flatDiscount || 0);
        $scope.amountToBePaid = ($scope.totalAmount - $scope.calculatedDiscount);
    }

    $scope.removeItem = function (index) {
        if($scope.allItems[index].quantity > 0){
          $scope.allItems[index].quantity = $scope.allItems[index].quantity - 1;
          $scope.refreshCheckoutCalculation();
        }
        $scope.cacheOrderItemData();
        $scope.removeSelectedSaleOffer();
    };

    $scope.getAllTaxes = function(){
        var data = {
            agentId:$rootScope.agentId,
            accessToken:$rootScope.accessToken
        };
        $rootScope.loader = true;
        $http.post(ENV().API_ENDPOINT+"v1/tax/get",data)
            .then(function sucess(data){
                console.log(data)
                $rootScope.loader = false;
                $scope.allTaxes = data.data.data
                for(var i in $scope.allTaxes){
                    $scope.allTaxes[i].amount = 0;
                }
                $scope.refreshCheckoutCalculation();
            }, function error(err){
                console.log(err);
                $rootScope.loader = false;
            })
    };


    $scope.applyPromoCode = function () {
      $scope.isDefaultPromoCodeApplied = false;
      $scope.calculatedDiscount = 0;
        var taxAmount = 0;
        for(var i in $scope.allTaxes){
            taxAmount += $scope.allTaxes[i].amount
        }

        var data = {
            promoName:$scope.promoCode,
            customerId:$scope.customer.id,
            totalAmount:$scope.totalAmount - taxAmount,
            agentId:$rootScope.agentId,
            accessToken:$rootScope.accessToken
        }


        $rootScope.loader = true;
        $http.post(ENV().API_ENDPOINT+"v1/promoCode/get",data)
            .then(function success(data){
                console.log(data);
                // $scope.promoCodeDiscount = data.data.data.amount;
                $scope.customerData.promoCode = data.data.data.promoCode;
                console.log("$scope.customerData.promoCode"+JSON.stringify($scope.customerData.promoCode));
                $scope.promoCodeDiscount = $scope.calculatePromoCodeAmount($scope.totalAmount-$scope.totalTaxAmount);
                $scope.isPromoCodeApplied = true;
                $rootScope.loader = false;
            },function error(err){
                swal("Promo not applicable");
                console.log(err);
                $scope.promoCode = "'"
                $scope.isPromoCodeApplied = false;
                $rootScope.loader = false;
                $scope.promoCodeDiscount = 0;
            })
    };


    $scope.removePromoCode = function(){
        $scope.promoCodeDiscount = 0;
        $scope.calculatedDiscount = 0;
        if (!$scope.isPromoCode) {
          $scope.isPromoCodeApplied = false;
        }

    };


  $scope.removeDefaultPromoCode = function () {
    $scope.isDefaultPromoCodeApplied = false;
    $scope.isPromoCodeApplied = true;
    $scope.calculatedDiscount = 0;
    $scope.promoCodeDiscount = 0;
  };

  $scope.calculatedOfferType = function (totalAmount, gstAmount) {
        if ($scope.offerSelected && ($scope.offerSelected.offerType === 5 || $scope.offerSelected.offerType === 3)) {
            if ($scope.offerSelected.discountType == 2) {
              discount = ((totalAmount - gstAmount) * ($scope.offerSelected.amount));
            } else if ($scope.offerSelected.discountType == 1) {
              discount = $scope.offerSelected.amount;
            }
        } else if($scope.offerSelected && $scope.offerSelected.offerType === 2) {
            discount = $scope.offerSelected.value;
        } else {
            discount = 0;
            console.log("outer else console.log(discount)" + discount);
        }
        return discount;
    }
  $scope.saleLoader = false;
  $scope.checkout = function() {
    $scope.saleLoader = true;
    if($scope.calculatedDiscount > $scope.totalAmount) {
        $scope.saleLoader = false;
        swal("Calculated Discount greater than total amount");
        return false;
    }
    if($scope.totalAmount > 0 || (isObjectNotEmpty($scope.offerSelected) && $scope.totalAmount == 0)) {

      var checkoutItems = [];

      for (var i in $scope.allItems) {
        if ($scope.allItems[i].quantity) {
          checkoutItems.push({itemID: $scope.allItems[i]._id, quantity: $scope.allItems[i].quantity});
        }
      }

      $scope.netTotalAmount = $scope.totalAmount - $scope.calculatedDiscount;

      var data = {
        agentId: Number($rootScope.agentId),
        accessToken: $rootScope.accessToken,
        customerId: $scope.customerData._id,
        totalAmount : $scope.totalAmount,
        amountPaid: Number($scope.amountToBePaid),
        dateOfTransaction:new Date(),
        itemCost: Number(Math.round($scope.totalAmount - $scope.totalTaxAmount)),
        totalTax: Number($scope.totalTaxAmount),
        items: JSON.stringify(checkoutItems),
        source: 3,
        notifyUser: $scope.sales.notifyUser,
        billNumber: ($scope.billNumber) ? $scope.billNumber : undefined
      };

     
        if($scope.offerSelected.offerType == 1 || $scope.offerSelected.offerType == 2) {
            data.cashbackId = Number($scope.offerSelected._id);
        } else {
            data.incentiveId = (Number($scope.offerSelected._id)) ? Number($scope.offerSelected._id) : -1;
        }


        $http.post(ENV().API_ENDPOINT + "v1/transaction/record", data)
            .then(function success(data) {
              $scope.transactionSpadesEarned = data.data.spadesEarned;
              $scope.transactionId = data.data._id;
              $scope.saleLoader = false;
              angular.element('#successfulTransactionModal').modal('show');
            }, function error(err) {
              console.log(err);
               $scope.saleLoader = false;
            })

        $scope.goToMerchant = function () {
            angular.element('#successfulTransactionModal').modal('hide');
            angular.element('#successfulTransactionModal .close').click();
            angular.element('#successfulTransactionModal').css('display', 'none');
            angular.element('.modal-backdrop').remove();
            var userAccountData = $cookies.getObject('userAccountData');
            updateTransactionsBills.get(userAccountData);
            if ($scope.salesBillPrint) {
              var divToPrint = document.getElementById('printThis');
              var htmlToPrint = '' +
                '<style type="text/css">' +
                'table th, table td {' +
                'border:0px ;' +
                'padding;0.5em;' +
                '}' +
                '</style>';
              htmlToPrint += divToPrint.outerHTML;
              var newWin = window.open("");
              newWin.document.write(htmlToPrint);
              newWin.print();
              newWin.close();
            }
          }
        } else {
          $scope.saleLoader = false;  
          swal("Checkout Empty");
        }
    };


    $scope.getAllItems();

    $scope.getAllTaxes();


    if(customerData.cashbacks){
        $scope.customeroffersData = customerData.cashbacks;

        $scope.showOffer = true;

        console.log("sasasasa asasa");
        console.log($scope.customeroffersData);

    }else {
        $scope.showOffer = false;
    }


    $scope.updateSelection = function(position, entities,offers) {

        console.log(position);
        console.log(offers);

        $scope.selectedOfferData = offers;


        angular.forEach(entities, function(subscription, index) {


            if (position != index)
                subscription.checked = false;
        });
    }

    $scope.updateSelection2 = function(position, entities,offers) {
        console.log(position);
        console.log(offers);

        $scope.selectedOfferData = offers;

        angular.forEach(entities, function(subscription, index) {

            if (position != index)
                subscription.checked = false;
        });
    };


    $scope.offerItemSelected = false;
    console.log('offerSelected', $window.sessionStorage.offerSelected);
    if($window.sessionStorage.offerSelected != undefined) {
        $scope.offerSelectedcust = true;
        $scope.offerSelected =  ($window.sessionStorage.offerSelected != '') ? JSON.parse($window.sessionStorage.offerSelected) : {};
        $scope.offerItemSelected = true;
    } else {
        $scope.offerSelectedcust = false;
        $scope.offerSelected ={};
    }

    $scope.addOffer = function () {

        billAmount.add($scope.totalAmount, $scope.spadesAvailable);
        $scope.custData = billAmount.list[0];
        $scope.billAmount = Number($scope.custData.newBillAmount);
        $location.path('/addOffer/2');

    };

    function isObjectNotEmpty(object) {
      return !(object === undefined || object === null || Object.keys(object).length === 0);
    }

    $scope.removeSelectedSaleOffer = function () {
        $scope.offerSelected = {};
        $scope.offerItemSelected = false;
        $scope.amoutToBeDetucted = 0;
        $scope.refreshCheckoutCalculation();
        $window.sessionStorage.offerSelected = '';
    }

  if (angular.fromJson(sessionStorage.getItem('billNumberData'))) {
    billNumberData = angular.fromJson(sessionStorage.getItem('billNumberData'));
    $scope.hideBillNoButton = true;
    $scope.billNumber = billNumberData.number;
    $scope.billNumberDataEnable = billNumberData.enable;
  }
  $scope.addSaleBillNo = function () {
    $scope.hideBillNoButton = true;

    //$scope.billNumberData = angular.fromJson(sessionStorage.getItem('billNumberData'));
  }

  $scope.updateProfile = function () {

    if ($scope.username) {
      var data = {
        customerId:$scope.customerData._id.toString(),
        firstName:$scope.username,
        email:$scope.customerData.Email,
        mobileNumber: $scope.customerData.MobileNumber,
        dateOfBirth: ($scope.customerData.DOB) ? new Date($scope.customerData.DOB) : undefined,
        marriageDate: ($scope.customerData.AnnivDate) ? new Date($scope.customerData.AnnivDate) : undefined,
        agentId:Number(customerCredentials.agentId),
        accessToken:customerCredentials.accessToken
      }

      $rootScope.loader = true;
      $http.post(ENV().API_ENDPOINT + "v1/customer/update",data)
        .then(function successCallback(response) {
          $rootScope.loader = false;
          sessionStorage.setItem('customerData', JSON.stringify(response));
          $scope.customerData.Name = $scope.username;
          angular.element('#profileUpdateModal').modal('hide');
          angular.element('#profileUpdateModal .close').click();
          angular.element('.modal-backdrop').remove();
          angular.element('#successfulProfileUpdateModal').modal('show');

        }, function errorCallback(response) {
          swal(data.message);
        })
    } else {
      swal("please fill  Name");
    }
  }

  $scope.openProfileUpdateForm = function () {

    $scope.username = $scope.customerData.Name;
    angular.element('#profileUpdateModal').modal('show');
  }

  $scope.notifyUserSet = function() {
    $window.sessionStorage.notifyUser = $scope.sales.notifyUser;
  }

  $scope.offerItemColor = ['blue','purple','green','orange','red','darkblue','darkpurple','darkgreen','darkorange','darkred'];

    $scope.offerRPItemColor = ['blue','purple','green','orange','red','darkblue','darkpurple','darkgreen','darkorange','darkred'];


    // $window.document

    $scope.$on('$routeChangeSuccess', function () {

        var dateNow = new Date();
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
            },
            maxDate : 'now',
            widgetPositioning: {
                horizontal: 'left',
                vertical: 'top'
            }
        });

       
        $("#datetimepicker1").on("dp.change", function (e) {
            $scope.customerData.DOB = e.date._d;
        });

        $('#datetimepicker2').datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
            },
            maxDate : 'now',
            widgetPositioning: {
                horizontal: 'right',
                vertical: 'top'
            }
        });
        $("#datetimepicker2").on("dp.change", function (e) {
           $scope.customerData.AnnivDate = e.date._d;
        });
        if($scope.customerData.DOB) {
            $('#datetimepicker1').data("DateTimePicker").date(moment($scope.customerData.DOB));
        }
        if($scope.customerData.AnnivDate) {
            $('#datetimepicker2').data("DateTimePicker").date(moment($scope.customerData.AnnivDate));
        }

    });
});