/**
 * Created by brijesh on 14/09/17.
 */

app.controller('CustomerController', function ($scope,$location,$http,ENV,$rootScope,$cookies,updateTransactionsBills,billAmount,$window) {

    var customerData = JSON.parse(sessionStorage.getItem('customerData'));

    if(!customerData){
        $location.path('/newTransaction')
    }

    var customerCredentials=$cookies.getObject('loginCreditialscookie');

  $scope.customerData = customerData.data;
  $scope.customerData.DOB = ($scope.customerData.DOB)? new Date($scope.customerData.DOB) : undefined;
  $scope.customerData.AnnivDate = ($scope.customerData.AnnivDate)? new Date($scope.customerData.AnnivDate) : undefined;

    console.log("$scope.customerData.promocode");
    console.log($scope.customerData.promoCode);
    var loginData = JSON.parse(sessionStorage.getItem('loginData'));


    if(!loginData){
        $location.path('/')
    }

    $scope.shopStoreTaxData = loginData.data.taxes;
    console.log("merchant taxes");
    console.log($scope.shopStoreTaxData);

    $scope.netAmount =0;
    var discount = 0;
    var totalTax = 0;
    var itemCost = 0;
    var promoAmount = 0;
    var taxAmount = 0;
    $scope.promoApplyforCust = false;
    console.log($scope.customerData, 'customerData');
    
    if($scope.customerData.isNewCustomer) {
        angular.element('#profileUpdateModal').modal('show');
    }
    
    $scope.calculatePromoCodeAmount = function (newBillAmount) {
        // console.log(newBillAmount);
        // console.log($scope.customerData);
        // console.log($scope.shopStoreTaxData);
        if ($scope.customerData.promoCode && ($scope.customerData.promoCode.mintTotalAmount <= newBillAmount)) {
        $scope.promoApplyforCust = true;

        //Calculate Item cost
        for (var tax in $scope.shopStoreTaxData) {
          totalTax += Number($scope.shopStoreTaxData[tax].taxPercent);
        }

        itemCost = newBillAmount / (totalTax + 1);  //item cost calculated
        console.log("itemCost   " + itemCost);
        taxAmount = newBillAmount - itemCost; //tax paid calculated
        console.log("taxAmount " + taxAmount);
        promoAmount = (newBillAmount) - (newBillAmount) / (Number($scope.customerData.promoCode.amount + 1)); //promoAmount calculated
        console.log("promoAmount" + promoAmount);

        if ($scope.customerData.promoCode.type == 1) {
          discount = Math.min($scope.customerData.promoCode.amount, itemCost);
          console.log("$scope.customerData.promoCode.type == 1 console.log(discount)" + discount);
        }
        else {
          console.log("promoAmount * itemCost"+$scope.customerData.promoCode.amount * itemCost);
          discount = Math.min(($scope.customerData.promoCode.amount * itemCost), itemCost);
          console.log("else console.log(discount)" + discount);
        }

      } else {
            discount = 0;
            console.log("outer else console.log(discount)" + discount);
        }
        return discount;
    }

    $scope.calculatedOfferType  = function  (newBillAmount) {
         console.log($scope.customerData);
        if ($scope.offerSelected && ($scope.offerSelected.offerType === 5 || $scope.offerSelected.offerType === 3)) {
             //Calculate Item cost
             var totalTax = 0;
            for (var tax in $scope.shopStoreTaxData) {
              totalTax += Number($scope.shopStoreTaxData[tax].taxPercent);
            }
            var itemCost = newBillAmount / (totalTax + 1);  //item cost calculated
            console.log("itemCost   " + itemCost);
            var taxAmount = newBillAmount - itemCost; //tax paid calculated
            console.log("taxAmount " + taxAmount);
            
            //&& ($scope.offerSelected.minAmount >= newBillAmount)
            if ($scope.offerSelected.discountType == 2) {
              discount = (itemCost * ($scope.offerSelected.amount ));
            } else if ($scope.offerSelected.discountType == 1) {
              discount = $scope.offerSelected.amount;
            }
        } else {
            discount = 0;
            console.log("outer else console.log(discount)" + discount);
        }
        return discount;
    }

    $scope.amountToPay = function (newBillAmount) {
        console.log("asasasasasssss");
        console.log(newBillAmount);
        var calculatedDiscount = Math.round($scope.calculatePromoCodeAmount(newBillAmount));
        var calculatedOfferTypeDiscount =  Math.round($scope.calculatedOfferType(newBillAmount));
        
        $scope.discountPromoAmount = calculatedDiscount;
        if($scope.amoutToBeDetucted && ($scope.amoutToBeDetucted <= $scope.netAmount)) {
           $scope.netAmount = newBillAmount - $scope.amoutToBeDetucted - calculatedDiscount - calculatedOfferTypeDiscount;
        } else {
           $scope.netAmount  = newBillAmount - calculatedDiscount - calculatedOfferTypeDiscount;
        }
    };

    if (sessionStorage.getItem('BillAmount')) {
        $scope.custData = JSON.parse(sessionStorage.getItem('BillAmount'));
        $scope.newBillAmount = $scope.custData.newBillAmount;
        console.log($scope.newBillAmount);
        $scope.amountToPay($scope.newBillAmount);
    }

    $scope.offerItemSelected = false;
    if($window.sessionStorage.offerSelected != undefined) {
        $scope.offerSelectedcust = true;
        $scope.offerSelected =  ($window.sessionStorage.offerSelected != '') ? JSON.parse($window.sessionStorage.offerSelected) : {};
        if($scope.offerSelected.offerType == 2){
            $scope.amoutToBeDetucted = Number($scope.offerSelected.value);
            $scope.amountToPay($scope.newBillAmount);
            $scope.offerItemSelected = true;
        } else {
            $scope.offerItemSelected = true;
            //swal("Total amount should be greater than 700, 1000");
            $scope.amountToPay($scope.newBillAmount);
        }
    }else {
        $scope.offerSelectedcust = false;
        $scope.offerSelected ={};
    }
    console.log("offer$scope.offerSelected");
    console.log($scope.offerSelected);

  $scope.removeSelectedoffer = function () {
    $window.sessionStorage.offerSelected = '';
    $scope.offerSelected = {};
    $scope.amoutToBeDetucted = 0;
    $scope.amountToPay($scope.newBillAmount);

  }

  if (angular.fromJson(sessionStorage.getItem('billNumberData'))) {
    billNumberData = angular.fromJson(sessionStorage.getItem('billNumberData'));
    $scope.hideBillNoButton = true;
    $scope.billNumber = billNumberData.number;
    $scope.billNumberDataEnable = billNumberData.enable;
  }
  $scope.addBillNo = function () {
    $scope.hideBillNoButton = true;
    $scope.billNumber = '';
    //$scope.billNumberData = angular.fromJson(sessionStorage.getItem('billNumberData'));
  }

  $scope.removeBillNo = function () {
    $scope.hideBillNoButton = false;
  }

    $scope.addOffer = function () {
      var billNumberData = {enable: true, number: $scope.billNumber };
      sessionStorage.setItem('billNumberData', angular.toJson(billNumberData));

        if($scope.newBillAmount){
            billAmount.add($scope.newBillAmount, $scope.customerData.spades);
            $location.path('/addOffer/1');
        }else {
            // swal("Please Submit Bill Amount");
            billAmount.add(0, $scope.customerData.spades);
            $location.path('/addOffer/1');
        }
    };

    $scope.paymentSuccessful = false;
    $scope.transactionLoader = false;
    $scope.addTransaction = function () {
        $scope.transactionLoader = true;
        if ($scope.netAmount > 0 || ($scope.offerSelected.offerType == 1)) {
            var data = {
                agentId: Number(customerCredentials.agentId),
                accessToken: customerCredentials.accessToken,
                customerId:$scope.customerData._id,
                totalAmount :$scope.newBillAmount,
                amountPaid:Number($scope.netAmount),
                dateOfTransaction:new Date(),
                itemCost:Number(Math.round(itemCost)),
                totalTax:Number(taxAmount),
                source:2,
                billNumber: ($scope.billNumber) ? $scope.billNumber : undefined
            };

            if($scope.customerData.promoCode){
                data.promoId = Number($scope.customerData.promoCode._id);
                data.promoCodeAmount = $scope.discountPromoAmount;
            }

            if($scope.offerSelected.offerType == 1 || $scope.offerSelected.offerType == 2){
                data.cashbackId = Number($scope.offerSelected._id);
            } else {
                data.incentiveId = (Number($scope.offerSelected._id)) ? Number($scope.offerSelected._id) : -1;
            }

            $http.post(ENV().API_ENDPOINT+"v1/transaction/record",data)
                .then(function successCallback(response){
                    $scope.successfulTransaction = response.data;
                    $scope.paymentSuccessful = true;
                    $scope.transactionLoader = false;
                    angular.element('#successfulTransactionModal').modal('show');
                },function errorCallback(err) {
                    $rootScope.loader = false;
                    $scope.transactionLoader = false;
                    swal("Please Enter Required Fields");
                });

        } else {
            $scope.transactionLoader = false;
            swal("Total amount should be greater than 0");
        }    
    };

    console.log("('userAccountData')")
    console.log($cookies.getObject('userAccountData'));

    $scope.goToMerchant = function () {
        console.log("userAccountData");
        var userAccountData = $cookies.getObject('userAccountData');
        angular.element('#successfulTransactionModal').modal('hide');
        angular.element('#successfulTransactionModal .close').click();
        angular.element('.modal-backdrop').remove();
        updateTransactionsBills.get(userAccountData);
    }

  /*$scope.refreshPage = function () {
    angular.element('#successfulProfileUpdateModal').modal('hide');
    angular.element('#successfulProfileUpdateModal .close').click();
    angular.element('.modal-backdrop').remove();

  }*/

  $scope.updateProfile = function () {

    if ($scope.username) {
      var data = {
        customerId:$scope.customerData._id.toString(),
        firstName:$scope.username,
        mobileNumber: $scope.customerData.MobileNumber,
        email:$scope.customerData.Email,
        dateOfBirth: ($scope.customerData.DOB) ? $scope.customerData.DOB : undefined,
        marriageDate: ($scope.customerData.AnnivDate) ? $scope.customerData.AnnivDate : undefined,
        agentId:Number(customerCredentials.agentId),
        accessToken:customerCredentials.accessToken
      }

      $rootScope.loader = true;
      $http.post(ENV().API_ENDPOINT + "v1/customer/update",data)
        .then(function successCallback(response) {
          $rootScope.loader = false;
          sessionStorage.setItem('customerData', JSON.stringify(response));
          $scope.customerData.Name = $scope.username;
          angular.element('#profileUpdateModal').modal('hide');
          angular.element('#profileUpdateModal .close').click();
          angular.element('.modal-backdrop').remove();
          angular.element('#successfulProfileUpdateModal').modal('show');

        }, function errorCallback(response) {
          swal(data.message);
        })
    } else {
      swal("please fill  Name");
    }
  }

  $scope.openProfileUpdateForm = function () {

    $scope.username = $scope.customerData.Name;
    angular.element('#profileUpdateModal').modal('show');
  }

  $scope.$on('$routeChangeSuccess', function () {
        var dateNow = new Date();
        $('#datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
            },
            maxDate : 'now',
            widgetPositioning: {
                horizontal: 'left',
                vertical: 'top'
            }
        });

       
        $("#datetimepicker1").on("dp.change", function (e) {
            $scope.customerData.DOB = e.date._d;
        });

        $('#datetimepicker2').datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
            },
            maxDate : 'now',
            widgetPositioning: {
                horizontal: 'right',
                vertical: 'top'
            }
        });
        $("#datetimepicker2").on("dp.change", function (e) {
           $scope.customerData.AnnivDate = e.date._d;
        });
        if($scope.customerData.DOB) {
            $('#datetimepicker1').data("DateTimePicker").date(moment($scope.customerData.DOB));
        }
        if($scope.customerData.AnnivDate) {
            $('#datetimepicker2').data("DateTimePicker").date(moment($scope.customerData.AnnivDate));
        }
    });

    $(document).ready(function() {
        // Select your input element.
        var number = document.getElementById('billamount');

        number.onkeydown = function(e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
              || (e.keyCode > 47 && e.keyCode < 58) 
              || e.keyCode == 8)) {
                return false;
            }
        }
        $('#example1').DataTable({
            "pageLength": 5,
            aaSorting: [[0, 'desc']],
            "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
            "fnDrawCallback" : function(oSettings) {
                $('.paginate_button.previous.disabled').css('display', 'none');
                $('.paginate_button.next.disabled').css('display', 'none');
                $('#example1_paginate').css('text-align', 'right');
            }
        });

    } );


});

