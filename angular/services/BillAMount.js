var app = angular.module('greenSpades');

app.factory('billAmount',function () {

    var messages = {};

    messages.list = [];

    messages.add = function(newBillAmount,spades){
        messages.list.pop();
        messages.list.push({
            newBillAmount: newBillAmount,
            spades :spades

        });
    };
    return messages;
});