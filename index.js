/**
 * Created by brijesh on 11/09/17.
 */


(function () {
    var app =  angular.module('greenSpades',['ngRoute','ngMaterial','datatables','ngCookies', 'ngSanitize'])
        .run(function ($log) {
            $log.debug("App running");

        });
})();