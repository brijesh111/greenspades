/**
 * Created by rajeshpatel on 25/11/17.
 */
'use strict';

var app = angular.module('greenSpades');

app.controller("ApiFeedbackController",function($scope,$http,$location,ENV) {
  var mobile = $location.search().a;
  var merchant_id = $location.search().b;
  $scope.phoneNumber = mobile;
  var track = angular.isDefined($location.search().t) ? $location.search().t : undefined;
  var trackingURL = $location.absUrl();

    if (track) {
        var data = { "mobileNumber": mobile, "merchantId": merchant_id, "type": track, "shortUrl": trackingURL };
        $http.post(ENV().API_ENDPOINT + "v1/link/track/add", data);
    }
  $scope.starClicked = starClicked;
  $scope.star = [];
  $scope.feedbackScale = [];
  function starClicked(event, index) {
    var $elem = angular.element(event.target);
    $scope.star[index] = (5 - $elem.index());
    console.log('rating',$scope.star);
  }

  $scope.isDataLoaded = false;
  $scope.isDataNotFound = false;
  $scope.isLoaded = true;

  $scope.getFeedbackParams= function(){
    $http.get(ENV().API_ENDPOINT+"v1/merchant/"+merchant_id+"/feedback/data")
      .then(function success(data){
        console.log(data);
        $scope.isDataLoaded = true;
        $scope.isLoaded = false;
        $scope.feedbackParams = data.data.data.feedbackParams;
        var feedbackScale = data.data.data.feedbackScale;
        for(var i=0; i < feedbackScale; i++) {
          $scope.feedbackScale.push(i);
          $scope.star[i] = 0;
        }


      }, function error(err){
        console.log(err);
        $scope.isDataLoaded = true;
        $scope.isDataNotFound = true;
        $scope.isLoaded = false;
      })
  };

  $scope.getFeedbackParams();

  $scope.submitFeedback  = function(){
  console.log('$scope', $scope);
    var feedback = [];
    for(var i in $scope.feedbackParams){
      var selectedValue = $scope.star[i];
      var selectedParam = $scope.feedbackParams[i];
      feedback.push({name:selectedParam,rating:selectedValue})
    }

    var data = {
      feedback:JSON.stringify(feedback),
      mobileNumber: $scope.phoneNumber,
        merchantId: merchant_id,
      comments: JSON.stringify($scope.feedbackComments)

    }
    console.log(data);
    $scope.loader = true;
    $http.post(ENV().API_ENDPOINT+"v1/feedback/add",data)
      .then(function success(data){
        $scope.loader = false;
        $scope.star = [];
        $scope.phoneNumber = "";
        $scope.feedbackComments = "";
        $location.path('/thankYou');

      },function error(err){
        $scope.loader = false;
        swal("Something went wrong!!")
      })

  };

});