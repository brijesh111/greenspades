/**
 * Created by brijesh on 14/09/17.
 */


app.controller('AddOfferController', function ($scope,$location,billAmount,$cookies,$window, $routeParams) {


    var cw = $('.child').width();
    $('.child').css({
        'height': cw + 'px'
    });

    var customerCredentials=$cookies.getObject('loginCreditialscookie');

    var customerData = JSON.parse(sessionStorage.getItem('customerData'));

    console.log('getcustomerDat', customerData);

    $scope.customeroffersData = customerData.data.offers;
    if($window.sessionStorage.offerSelected != undefined) {
        $scope.selectedOfferData =  ($window.sessionStorage.offerSelected != '') ? JSON.parse($window.sessionStorage.offerSelected) : {};
        angular.forEach($scope.customeroffersData, function(offer, index) {
            if (offer._id == $scope.selectedOfferData._id) {
                offer.checked = true;
            }
        });
    }

    var data = billAmount.list;
    if (data.length) {
        $scope.custData = data[0];

        sessionStorage.setItem('BillAmount',JSON.stringify($scope.custData))
        // $window.sessionStorage.BillAmount =  JSON.stringify($scope.custData);
    } else if (sessionStorage.getItem('BillAmount')) {
        $scope.custData = JSON.parse(sessionStorage.getItem('BillAmount'));
    } else{
        $location.path('/');
        return;
    }
    $scope.billAmount = Number($scope.custData.newBillAmount);
    $scope.updateSelection = function(position, entities,offers) {
        $scope.selectedOfferData = offers;
        angular.forEach(entities, function(subscription, index) {
            if (position != index)
                subscription.checked = false;
        });
    }

    $scope.applyPromo = function () {
        if($scope.selectedOfferData){
            $window.sessionStorage.offerSelected = JSON.stringify($scope.selectedOfferData);
        }
        if($routeParams.redirectpath == 1) {
            $location.path('/customer');
        } else {
            $location.path('/makeSale');
        }
        
    }
    $scope.offerItemColor = ['#8E532F','#4F68C4','#349D99','#329567','#92A136','#8A7F2E','#997433','#346E9D','#507A29','#A45437'];
    $scope.offerRPItemColor = ['#8E532F','#4F68C4','#349D99','#329567','#92A136','#8A7F2E','#997433','#346E9D','#507A29','#A45437'];
});
