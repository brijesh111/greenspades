/**
 * Created by brijesh on 16/09/17.
 */

var app = angular.module('greenSpades');


app.controller('NavigationController', function ($scope,$location,$rootScope,$http,ENV) {

  $scope.homePageURL = ($location.path() == '/newTransaction') ? true : false;

  $scope.logout = function () {

      // sessionStorage.removeItem('key');
      sessionStorage.clear();
      $location.path('/');

  };

  $scope.home = function () {

      if(sessionStorage.getItem('loginData')){
          $location.path('/newTransaction')
      }else {
          $location.path('/');
      }
  }

});
