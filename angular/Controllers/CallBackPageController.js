'use strict';

var app = angular.module('greenSpades');

app.controller("CallBackPageController",function($scope) {
  $scope.$on('$locationChangeStart', function(event, next, current){
    event.preventDefault();
  });
});
